# Symphony ConsulTI Template

### Es importante no cambiar los archivos de configuración ya establecidos: No cambiar puertos de salida

Este es un proyecto para demostrar el uso de la herramienta Symphony

## Para comenzar:

### Clonar el repositorio

```shell
git clone https://github.com/ErickEspinozaT/consulti-template-symphony.git
```

### Instalar paquetes npm

Instalar los paquetes y dependencias de composer e iniciar el proyecto:

```shell
curl -Ss getcomposer.org/installer | php
chmod +x composer.phar -> linux
composer require symfony/requirements-checker
composer install
composer require --dev symfony/web-server-bundle
php bin/console server:start
```

El comando `server:start` arranca la aplicación, y se accede a traves del puerto `8000`

Para apagar el server manualmente usar `Ctrl+C`
