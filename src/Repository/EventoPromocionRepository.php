<?php

namespace App\Repository;

use App\Entity\EventoPromocion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventoPromocion|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventoPromocion|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventoPromocion[]    findAll()
 * @method EventoPromocion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventoPromocionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventoPromocion::class);
    }

    // /**
    //  * @return EventoPromocion[] Returns an array of EventoPromocion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventoPromocion
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
