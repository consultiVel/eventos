<?php

namespace App\Form;

use App\Entity\Evento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Required;


class EventoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nombre',
                TextType::class,
                [
                    'constraints' => [
                        new NotNull(['message' => 'Nombre requerido']),
                        new Required()
                    ]
                ]
            )
            ->add(
                'fecha_evento',
                DateType::class,
                [
                    'constraints' =>  [
                        new NotNull(['message' => 'Fecha del requerido']),
                        new Required()
                    ]
                ]
            )
            ->add('empresario', DateType::class, [
                'constraints' =>  [
                    new NotNull(['message' => 'Empresario requerido']),
                    new Required()
                ]
            ])
            ->add('es_publico', IntegerType::class, [
                'constraints' =>  [
                    new NotNull(['message' => 'Ver si el estado ']),
                    new Required()
                ]
            ])
            ->add('precio', IntegerType::class, [
                'constraints' =>  [
                    new NotNull(['message' => 'Precio requerido']),
                    new Required()
                ]
            ])
            ->add('cantidad_entradas', IntegerType::class, [
                'constraints' =>  [
                    new NotNull(['message' => 'Entadas requeridas']),
                    new Required()
                ]
            ])
            ->add('estado', IntegerType::class, [
                'constraints' =>  [
                    new NotNull(['message' => 'Estado  requerido']),
                    new Required()
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evento::class,
            'csrf_protection' => false
        ]);
    }
}
