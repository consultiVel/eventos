<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EventoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=EventoRepository::class)
 */
class Evento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_evento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $empresario;

    /**
     * @ORM\Column(type="integer")
     */
    private $es_publico;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantidad_entradas;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_creacion;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_actualizacion;

    /**
     * @ORM\ManyToMany(targetEntity=Promocion::class, inversedBy="evento_id")
     */
    private $Promocion;

    public function __construct()
    {
        $this->Promocion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaEvento(): ?\DateTimeInterface
    {
        return $this->fecha_evento;
    }

    public function setFechaEvento(\DateTimeInterface $fecha_evento): self
    {
        $this->fecha_evento = $fecha_evento;

        return $this;
    }

    public function getEmpresario(): ?string
    {
        return $this->empresario;
    }

    public function setEmpresario(string $empresario): self
    {
        $this->empresario = $empresario;

        return $this;
    }

    public function getEsPublico(): ?int
    {
        return $this->es_publico;
    }

    public function setEsPublico(int $es_publico): self
    {
        $this->es_publico = $es_publico;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getCantidadEntradas(): ?int
    {
        return $this->cantidad_entradas;
    }

    public function setCantidadEntradas(int $cantidad_entradas): self
    {
        $this->cantidad_entradas = $cantidad_entradas;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fecha_creacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fecha_creacion): self
    {
        $this->fecha_creacion = $fecha_creacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fecha_actualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fecha_actualizacion): self
    {
        $this->fecha_actualizacion = $fecha_actualizacion;

        return $this;
    }

    /**
     * @return Collection|Promocion[]
     */
    public function getPromocion(): Collection
    {
        return $this->Promocion;
    }

    public function addPromocion(Promocion $promocion): self
    {
        if (!$this->Promocion->contains($promocion)) {
            $this->Promocion[] = $promocion;
        }

        return $this;
    }

    public function removePromocion(Promocion $promocion): self
    {
        $this->Promocion->removeElement($promocion);

        return $this;
    }
}
