<?php

namespace App\Controller;

use App\Repository\EventoRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Evento;
use App\Form\EventoType;

/**
 * @Route("/api")
 */
class EventoController extends AbstractApiController
{

    function __construct(EventoRepository $eventoRepository)
    {
        $this->eventoRepository = $eventoRepository;
    }

    /**
     * @Route("/eventos", name="evento" , methods={"GET"})
     */
    public function index(): Response
    {

        $eventos =  $this->eventoRepository->findAll();
        return $this->json(['data' => $eventos], 200);
    }


    /**
     * @Route("/eventos", name="evento_new", methods={"POST"})
     */
    public function save(Request $request): Response
    {
        $evento = new Evento();

        $form = $this->buildForm(EventoType::class);
        $form->handleRequest($request);
        

      

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Evento $evento */
            $evento = $form->getData();

            $this->getDoctrine()->getManager()->persist($evento);
            $this->getDoctrine()->getManager()->flush();

             //get Evento 
             $response = $this->eventoRepository->find($evento);
             return $this->respond(['data' => $response]);

        }

        return $this->respond($form, Response::HTTP_BAD_REQUEST);
    }
}
