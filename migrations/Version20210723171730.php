<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723171730 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE evento_promocion_evento');
        $this->addSql('DROP TABLE evento_promocion_promocion');
        $this->addSql('ALTER TABLE evento CHANGE fecha_creacion fecha_creacion DATETIME NOT NULL, CHANGE fecha_actualizacion fecha_actualizacion DATETIME NOT NULL');
        $this->addSql('ALTER TABLE evento_promocion MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE evento_promocion DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE evento_promocion ADD evento_id INT NOT NULL, ADD promocion_id INT NOT NULL, DROP id, DROP fecha_creacion, DROP fecha_actualizacion');
        $this->addSql('ALTER TABLE evento_promocion ADD CONSTRAINT FK_31CB21D987A5F842 FOREIGN KEY (evento_id) REFERENCES evento (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion ADD CONSTRAINT FK_31CB21D9B1E453D4 FOREIGN KEY (promocion_id) REFERENCES promocion (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_31CB21D987A5F842 ON evento_promocion (evento_id)');
        $this->addSql('CREATE INDEX IDX_31CB21D9B1E453D4 ON evento_promocion (promocion_id)');
        $this->addSql('ALTER TABLE evento_promocion ADD PRIMARY KEY (evento_id, promocion_id)');
        $this->addSql('ALTER TABLE promocion CHANGE fecha_creacion fecha_creacion DATETIME NOT NULL, CHANGE fecha_actualizacion fecha_actualizacion DATETIME NOT NULL');
        $this->addSql('ALTER TABLE usuario CHANGE fecha_creacion fecha_creacion DATETIME NOT NULL, CHANGE fecha_actualizacion fecha_actualizacion DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evento_promocion_evento (evento_promocion_id INT NOT NULL, evento_id INT NOT NULL, INDEX IDX_9A4C3A8D6430C565 (evento_promocion_id), INDEX IDX_9A4C3A8D87A5F842 (evento_id), PRIMARY KEY(evento_promocion_id, evento_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE evento_promocion_promocion (evento_promocion_id INT NOT NULL, promocion_id INT NOT NULL, INDEX IDX_5E19740E6430C565 (evento_promocion_id), INDEX IDX_5E19740EB1E453D4 (promocion_id), PRIMARY KEY(evento_promocion_id, promocion_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE evento_promocion_evento ADD CONSTRAINT FK_9A4C3A8D6430C565 FOREIGN KEY (evento_promocion_id) REFERENCES evento_promocion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion_evento ADD CONSTRAINT FK_9A4C3A8D87A5F842 FOREIGN KEY (evento_id) REFERENCES evento (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion_promocion ADD CONSTRAINT FK_5E19740E6430C565 FOREIGN KEY (evento_promocion_id) REFERENCES evento_promocion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion_promocion ADD CONSTRAINT FK_5E19740EB1E453D4 FOREIGN KEY (promocion_id) REFERENCES promocion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento CHANGE fecha_creacion fecha_creacion DATETIME NOT NULL, CHANGE fecha_actualizacion fecha_actualizacion DATETIME NOT NULL');
        $this->addSql('ALTER TABLE evento_promocion DROP FOREIGN KEY FK_31CB21D987A5F842');
        $this->addSql('ALTER TABLE evento_promocion DROP FOREIGN KEY FK_31CB21D9B1E453D4');
        $this->addSql('DROP INDEX IDX_31CB21D987A5F842 ON evento_promocion');
        $this->addSql('DROP INDEX IDX_31CB21D9B1E453D4 ON evento_promocion');
        $this->addSql('ALTER TABLE evento_promocion ADD id INT AUTO_INCREMENT NOT NULL, ADD fecha_creacion DATETIME NOT NULL, ADD fecha_actualizacion DATETIME NOT NULL, DROP evento_id, DROP promocion_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE promocion CHANGE fecha_creacion fecha_creacion DATETIME NOT NULL, CHANGE fecha_actualizacion fecha_actualizacion DATETIME NOT NULL');
        $this->addSql('ALTER TABLE usuario CHANGE fecha_creacion fecha_creacion DATETIME NOT NULL, CHANGE fecha_actualizacion fecha_actualizacion DATETIME NOT NULL');
    }
}
