<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723170607 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evento (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, fecha_evento DATE NOT NULL, empresario VARCHAR(255) NOT NULL, es_publico INT NOT NULL, precio DOUBLE PRECISION NOT NULL, cantidad_entradas INT NOT NULL, estado INT NOT NULL, fecha_creacion DATETIME NOT NULL, fecha_actualizacion DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evento_promocion (id INT AUTO_INCREMENT NOT NULL, fecha_creacion DATETIME NOT NULL, fecha_actualizacion DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evento_promocion_evento (evento_promocion_id INT NOT NULL, evento_id INT NOT NULL, INDEX IDX_9A4C3A8D6430C565 (evento_promocion_id), INDEX IDX_9A4C3A8D87A5F842 (evento_id), PRIMARY KEY(evento_promocion_id, evento_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evento_promocion_promocion (evento_promocion_id INT NOT NULL, promocion_id INT NOT NULL, INDEX IDX_5E19740E6430C565 (evento_promocion_id), INDEX IDX_5E19740EB1E453D4 (promocion_id), PRIMARY KEY(evento_promocion_id, promocion_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promocion (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, descuento INT NOT NULL, fecha_inicio DATE NOT NULL, fecha_fin DATE NOT NULL, fecha_creacion DATETIME NOT NULL, fecha_actualizacion DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, usuario VARCHAR(255) NOT NULL, correo VARCHAR(255) NOT NULL, clave VARCHAR(255) NOT NULL, fecha_creacion DATETIME NOT NULL, fecha_actualizacion DATETIME NOT NULL, rol VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evento_promocion_evento ADD CONSTRAINT FK_9A4C3A8D6430C565 FOREIGN KEY (evento_promocion_id) REFERENCES evento_promocion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion_evento ADD CONSTRAINT FK_9A4C3A8D87A5F842 FOREIGN KEY (evento_id) REFERENCES evento (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion_promocion ADD CONSTRAINT FK_5E19740E6430C565 FOREIGN KEY (evento_promocion_id) REFERENCES evento_promocion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evento_promocion_promocion ADD CONSTRAINT FK_5E19740EB1E453D4 FOREIGN KEY (promocion_id) REFERENCES promocion (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evento_promocion_evento DROP FOREIGN KEY FK_9A4C3A8D87A5F842');
        $this->addSql('ALTER TABLE evento_promocion_evento DROP FOREIGN KEY FK_9A4C3A8D6430C565');
        $this->addSql('ALTER TABLE evento_promocion_promocion DROP FOREIGN KEY FK_5E19740E6430C565');
        $this->addSql('ALTER TABLE evento_promocion_promocion DROP FOREIGN KEY FK_5E19740EB1E453D4');
        $this->addSql('DROP TABLE evento');
        $this->addSql('DROP TABLE evento_promocion');
        $this->addSql('DROP TABLE evento_promocion_evento');
        $this->addSql('DROP TABLE evento_promocion_promocion');
        $this->addSql('DROP TABLE promocion');
        $this->addSql('DROP TABLE usuario');
    }
}
