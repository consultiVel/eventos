# Dockerfile
FROM php:8.0.3

RUN apt-get update && apt-get install -y telnet nano vim   libfreetype6-dev   libjpeg62-turbo-dev   libmcrypt-dev   libpng-dev   zlib1g-dev   libxml2-dev   libzip-dev   libonig-dev   graphviz   && docker-php-ext-configure gd   && docker-php-ext-install -j$(nproc) gd   && docker-php-ext-install pdo_mysql   && docker-php-ext-install mysqli   && docker-php-ext-install zip   && docker-php-source delete

WORKDIR /app
COPY . /app

EXPOSE 8000
CMD php bin/console server:run 0.0.0.0:8000